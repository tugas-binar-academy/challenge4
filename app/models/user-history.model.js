const pool = require("./db.js");

// constructor
const UserHistory = function (history) {
  this.id_history = history.id_history;
  this.id_user = history.id_user;
  this.id_game = history.id_game;
  this.id_result = history.id_result;
};

UserHistory.create = (newUserHistory, result) => {
  pool.query("INSERT INTO user_game_history (id_history, id_user, id_game, id_result) values ((SELECT MAX(id_history)+1 FROM user_game_history), $1, $2, $3)",
    [newUserHistory.id_user, newUserHistory.id_game, newUserHistory.id_result],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      console.log("created user game history: ", { id: res.insertId, ...newUserHistory });
      result(null, { id: res.insertId, ...newUserHistory });
    });
};

UserHistory.findById = (historyId, result) => {
  pool.query(`select user_game_history.id_history, user_game_history.id_user, username, game_name, result_name
	from user_game_history
	left join user_game on user_game_history.id_user = user_game.id_user
	left join games on user_game_history.id_game = games.id_game
	left join game_results on user_game_history.id_result = game_results.id_results where id_history = ${historyId}`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.rowCount !== 0) {
        console.log("found history: ", res);
        result(null, res);
        return;
      }

      // not found history with the id
      result({ kind: "not_found" }, null);
    });
};


UserHistory.getAll = result => {
  pool.query(`select user_game_history.id_history, user_game_history.id_user, username, game_name, result_name
    from user_game_history
    left join user_game on user_game_history.id_user = user_game.id_user
    left join games on user_game_history.id_game = games.id_game
    left join game_results on user_game_history.id_result = game_results.id_results order by id_history`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      console.log("user_game_history: ", res);
      result(null, res);
    });
};

UserHistory.updateById = (id, history, result) => {
  pool.query(
    "UPDATE user_game_history SET id_user = $1, id_history = $2, id_game = $3, id_result = $4 WHERE id_history = $5",
    [history.id_user, history.id_history, history.id_game, history.id_result, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.rowCount === 0) {
        // not found history with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated history: ", { id_history: id, ...history });
      result(null, { id_history: id, ...history });
    }
  );
};

UserHistory.remove = (id, result) => {
  pool.query("DELETE FROM user_game_history WHERE id_history = $1", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.rowCount === 0) {
      // not found history with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted history with id_history: ", id);
    result(null, res);
  });
};

UserHistory.removeAll = result => {
  pool.query("DELETE FROM user_game_history", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.row} user_game_history`);
    result(null, res);
  });
};

module.exports = UserHistory;