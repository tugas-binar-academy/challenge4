/**
 * @swagger
 * /games:
 *   get:
 *     tags: [Games]
 *     name: Find game
 *     summary: Finds all game
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Games object
 *       '401':
 *         description: no game found in db with that name
 *       '403':
 *         description: game from client don't match
 * 
 * /games/{id}:
 *   get:
 *     summary: gets games by id
 *     tags: [Games]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of game
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: games by its id
 *         content:
 *           application/json:
 *       400:
 *         description: game can not be found
 * /games/{id}/delete:
 *   delete:
 *     summary: remove games by id
 *     tags: [Games]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of game
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: games by its id
 *         content:
 *           application/json:
 *       400:
 *         description: game can not be found
 * /games/add:
 *   post:
 *     summary: add games
 *     tags: [Games]
 *     parameters:
 *      - in: body
 *        name: game
 *        schema:
 *          type: object
 *          required:
 *            - game_name
 *          properties:
 *            game_name:
 *              type: string
 *     responses:
 *       200:
 *         description: add games success
 *         content:
 *           application/json:
 *       400:
 *         description: game can not be found
 *       403:
 *         description: bad request
 * /games/{id}/update:
 *   put:
 *     summary: update games
 *     tags: [Games]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *      - in: body
 *        name: game
 *        schema:
 *          type: object
 *          required:
 *            - id_game
 *            - game_name
 *          properties:
 *            id_game:
 *              type: integer
 *            game_name:
 *              type: string
 *     responses:
 *       200:
 *         description: update games success
 *         content:
 *           application/json:
 *       400:
 *         description: game can not be found
 */


module.exports = app => {
  const games = require("../controllers/games.controller.js");

  // Create a new game
  app.post("/games/add", games.create);

  // Retrieve all games
  app.get("/games", games.findAll);

  // Retrieve a single game with gameId
  app.get("/games/:gameId", games.findOne);

  // Update a game with gameId
  app.put("/games/:gameId/update", games.update);

  // Delete a game with gameId
  app.delete("/games/:gameId/delete", games.delete);

  // delete all games
  app.delete("/games", games.deleteAll);
};