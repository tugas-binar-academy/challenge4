/**
 * @swagger
 * /gameResults:
 *   get:
 *     tags: [GameResults]
 *     name: Find game
 *     summary: Finds all game results
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Game results object
 *       '401':
 *         description: no game results found in db with that name
 *       '403':
 *         description: game results from client don't match
 * 
 * /gameResults/{id}:
 *   get:
 *     summary: gets game results by id
 *     tags: [GameResults]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of game results
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: game results by its id
 *         content:
 *           application/json:
 *       400:
 *         description: game results can not be found
 * /gameResults/{id}/delete:
 *   delete:
 *     summary: remove game results by id
 *     tags: [GameResults]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of game results
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: game results by its id
 *         content:
 *           application/json:
 *       400:
 *         description: game results can not be found
 * /gameResults/add:
 *   post:
 *     summary: add game results
 *     tags: [GameResults]
 *     parameters:
 *      - in: body
 *        name: game results
 *        schema:
 *          type: object
 *          required:
 *            - result_name
 *          properties:
 *            result_name:
 *              type: string
 *     responses:
 *       200:
 *         description: add game results success
 *         content:
 *           application/json:
 *       400:
 *         description: game results can not be found
 *       403:
 *         description: bad request
 * /gameResults/{id}/update:
 *   put:
 *     summary: update game results
 *     tags: [GameResults]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *      - in: body
 *        name: game
 *        schema:
 *          type: object
 *          required:
 *            - id_results
 *            - result_name
 *          properties:
 *            id_results:
 *              type: integer
 *            result_name:
 *              type: string
 *     responses:
 *       200:
 *         description: update game results success
 *         content:
 *           application/json:
 *       400:
 *         description: game can not be found
 */


module.exports = app => {
  const gameResult = require("../controllers/game-results.controller.js");

  // Create a new game
  app.post("/gameResults/add", gameResult.create);

  // Retrieve all gameResult
  app.get("/gameResults", gameResult.findAll);

  // Retrieve a single game with gameResultId
  app.get("/gameResults/:gameResultId", gameResult.findOne);

  // Update a game with gameResultId
  app.put("/gameResults/:gameResultId/update", gameResult.update);

  // Delete a game with gameResultId
  app.delete("/gameResults/:gameResultId/delete", gameResult.delete);

  // delete all gameResult
  app.delete("/gameResult", gameResult.deleteAll);
};