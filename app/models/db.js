const mysql = require("mysql");
const { Pool, Client } = require('pg');
const dbConfig = require("../config/db.config.js");

const pool = new Pool({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

// open the Postgres pool
pool.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

module.exports = pool;