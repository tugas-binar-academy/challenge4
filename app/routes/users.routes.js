/**
 * @swagger
 * /users:
 *   get:
 *     tags: [Users]
 *     name: Find user
 *     summary: Finds all user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Users object
 *       '401':
 *         description: No auth token / no user found in db with that name
 *       '403':
 *         description: JWT token and uraianMinuman from client don't match
 * 
 * /users/login/email/{email}/password/{password}:
 *   get:
 *     summary: login user
 *     tags: [Users]
 *     parameters:
 *       - in : path
 *         name: email
 *         description: email
 *         schema:
 *           type: string
 *         required: true
 *       - in : path
 *         name: password
 *         description: password
 *         schema:
 *           type: string
 *         required: true
 *     responses:
 *       200:
 *         description: users by its id
 *         content:
 *           application/json:
 *       400:
 *         description: user can not be found
 * /users/{id}:
 *   get:
 *     summary: gets users by id
 *     tags: [Users]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of user
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: users by its id
 *         content:
 *           application/json:
 *       400:
 *         description: user can not be found
 * /users/{id}/delete:
 *   delete:
 *     summary: remove users by id
 *     tags: [Users]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of user
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: users by its id
 *         content:
 *           application/json:
 *       400:
 *         description: user can not be found
 * /users/add:
 *   post:
 *     summary: add users
 *     tags: [Users]
 *     parameters:
 *      - in: body
 *        name: user
 *        schema:
 *          type: object
 *          required:
 *            - username
 *            - email
 *            - password
 *          properties:
 *            id_user:
 *              type: integer
 *            username:
 *              type: string
 *            email:
 *              type: string
 *            password:
 *              type: string
 *     responses:
 *       200:
 *         description: add users success
 *         content:
 *           application/json:
 *       400:
 *         description: user can not be found
 *       403:
 *         description: bad request
 * /users/{id}/update:
 *   put:
 *     summary: update users
 *     tags: [Users]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *      - in: body
 *        name: user
 *        schema:
 *          type: object
 *          required:
 *            - username
 *            - email
 *          properties:
 *            id_user:
 *              type: integer
 *            username:
 *              type: string
 *            email:
 *              type: string
 *     responses:
 *       200:
 *         description: update users success
 *         content:
 *           application/json:
 *       400:
 *         description: user can not be found
 */


module.exports = app => {
  const users = require("../controllers/users.controller.js");

  // Create a new user
  app.post("/users/add", users.create);

  // Retrieve all users
  app.get("/users", users.findAll);

  // Retrieve login users
  app.get("/users/login/email/:email/password/:password", users.loginUser);

  // Retrieve a single user with userId
  app.get("/users/:userId", users.findOne);

  // Update a user with userId
  app.put("/users/:userId/update", users.update);

  // Delete a user with userId
  app.delete("/users/:userId/delete", users.delete);

  // delete all users
  app.delete("/users", users.deleteAll);
};