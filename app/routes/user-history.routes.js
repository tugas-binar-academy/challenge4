/**
 * @swagger
 * /userHistorys:
 *   get:
 *     tags: [UserHistorys]
 *     name: Find userHistory
 *     summary: Finds all userHistory
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: UserHistorys object
 *       '401':
 *         description: no userHistory found in db with that name
 *       '403':
 *         description: userHistory from client don't match
 * 
 * /userHistorys/{id}:
 *   get:
 *     summary: gets userHistorys by id
 *     tags: [UserHistorys]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of userHistory
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: userHistorys by its id
 *         content:
 *           application/json:
 *       400:
 *         description: userHistory can not be found
 * /userHistorys/{id}/delete:
 *   delete:
 *     summary: remove userHistorys by id
 *     tags: [UserHistorys]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of userHistory
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: userHistorys by its id
 *         content:
 *           application/json:
 *       400:
 *         description: userHistory can not be found
 * /userHistorys/add:
 *   post:
 *     summary: add userHistorys
 *     tags: [UserHistorys]
 *     parameters:
 *      - in: body
 *        name: userHistory
 *        schema:
 *          type: object
 *          required:
 *            - id_user
 *            - id_game
 *            - id_result
 *          properties:
 *            id_user:
 *              type: integer
 *            id_game:
 *              type: integer
 *            id_result:
 *              type: integer
 *     responses:
 *       200:
 *         description: add userHistorys success
 *         content:
 *           application/json:
 *       400:
 *         description: userHistory can not be found
 *       403:
 *         description: bad request
 * /userHistorys/{id}/update:
 *   put:
 *     summary: update userHistorys
 *     tags: [UserHistorys]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *      - in: body
 *        name: userHistory
 *        schema:
 *          type: object
 *          required:
 *            - id_history
 *            - id_user
 *            - id_game
 *            - id_result
 *          properties:
 *            id_history:
 *              type: integer
 *            id_user:
 *              type: integer
 *            id_game:
 *              type: integer
 *            id_result:
 *              type: integer
 *     responses:
 *       200:
 *         description: update userHistorys success
 *         content:
 *           application/json:
 *       400:
 *         description: userHistory can not be found
 */


module.exports = app => {
  const userHistorys = require("../controllers/user-history.controller.js");

  // Create a new userHistory
  app.post("/userHistorys/add", userHistorys.create);

  // Retrieve all userHistorys
  app.get("/userHistorys", userHistorys.findAll);

  // Retrieve a single userHistory with userHistoryId
  app.get("/userHistorys/:userHistoryId", userHistorys.findOne);

  // Update a userHistory with userHistoryId
  app.put("/userHistorys/:userHistoryId/update", userHistorys.update);

  // Delete a userHistory with userHistoryId
  app.delete("/userHistorys/:userHistoryId/delete", userHistorys.delete);

  // delete all userHistorys
  app.delete("/userHistorys", userHistorys.deleteAll);
};