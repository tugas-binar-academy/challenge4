const pool = require("./db.js");

// constructor
const UserBiodata = function (biodata) {
  this.id_biodata = biodata.id_biodata;
  this.id_user = biodata.id_user;
  this.fullname = biodata.fullname;
  this.bio = biodata.bio;
};

UserBiodata.create = (newUserBiodata, result) => {
  pool.query("INSERT INTO user_game_biodata (id_biodata, id_user, fullname, bio) values ((SELECT MAX(id_biodata)+1 FROM user_game_biodata), $1, $2, $3)",
    [newUserBiodata.id_user, newUserBiodata.fullname, newUserBiodata.bio],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      console.log("created user game biodata: ", { id: res.insertId, ...newUserBiodata });
      result(null, { id: res.insertId, ...newUserBiodata });
    });
};

UserBiodata.findById = (biodataId, result) => {
  pool.query(`SELECT id_biodata, id_user, fullname, bio FROM user_game_biodata WHERE id_biodata = ${biodataId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.rowCount !== 0) {
      console.log("found biodata: ", res);
      result(null, res);
      return;
    }

    // not found biodata with the id
    result({ kind: "not_found" }, null);
  });
};


UserBiodata.getAll = result => {
  pool.query("SELECT id_biodata, id_user, fullname, bio FROM user_game_biodata order by id_biodata", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("user_game_biodata: ", res);
    result(null, res);
  });
};

UserBiodata.updateById = (id, biodata, result) => {
  pool.query(
    "UPDATE user_game_biodata SET id_user = $1, id_biodata = $2, fullname = $3, bio = $4 WHERE id_biodata = $5",
    [biodata.id_user, biodata.id_biodata, biodata.fullname, biodata.bio, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.rowCount === 0) {
        // not found biodata with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated biodata: ", { id_biodata: id, ...biodata });
      result(null, { id_biodata: id, ...biodata });
    }
  );
};

UserBiodata.remove = (id, result) => {
  pool.query("DELETE FROM user_game_biodata WHERE id_biodata = $1", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.rowCount === 0) {
      // not found biodata with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted biodata with id_biodata: ", id);
    result(null, res);
  });
};

UserBiodata.removeAll = result => {
  pool.query("DELETE FROM user_game_biodata", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.row} user_game_biodata`);
    result(null, res);
  });
};

module.exports = UserBiodata;