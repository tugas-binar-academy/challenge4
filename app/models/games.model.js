const pool = require("./db.js");

// constructor
const Games = function (game) {
  this.id_game = game.id_game;
  this.game_name = game.game_name;
};

Games.create = (newGames, result) => {
  pool.query("INSERT INTO games (id_game, game_name) values ((SELECT MAX(id_game)+1 FROM games), $1)", [newGames.game_name], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created game: ", { id: res.insertId, ...newGames });
    result(null, { id: res.insertId, ...newGames });
  });
};

Games.findById = (gameId, result) => {
  pool.query(`SELECT id_game, game_name FROM games WHERE id_game = ${gameId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.rowCount !== 0) {
      console.log("found game: ", res);
      result(null, res);
      return;
    }

    // not found game with the id
    result({ kind: "not_found" }, null);
  });
};


Games.getAll = result => {
  pool.query("SELECT id_game, game_name FROM games order by id_game", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("games: ", res);
    result(null, res);
  });
};

Games.updateById = (id, game, result) => {
  pool.query(
    "UPDATE games SET game_name = $1, id_game = $2 WHERE id_game = $3",
    [game.game_name, game.id_game, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.rowCount === 0) {
        // not found game with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated game: ", { id_game: id, ...game });
      result(null, { id_game: id, ...game });
    }
  );
};

Games.remove = (id, result) => {
  pool.query("DELETE FROM games WHERE id_game = $1", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.rowCount === 0) {
      // not found game with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted game with id_game: ", id);
    result(null, res);
  });
};

Games.removeAll = result => {
  pool.query("DELETE FROM games", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.row} games`);
    result(null, res);
  });
};

module.exports = Games;