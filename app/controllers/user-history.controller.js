const UserHistory = require("../models/user-history.model.js");

// Create and Save a new userHistory
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a userHistory
  const userHistory = new UserHistory({
    id_history: req.body.id_history,
    id_user: req.body.id_user,
    id_game: req.body.id_game,
    id_result: req.body.id_result,
  });

  // Save UserHistory in the database
  UserHistory.create(userHistory, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the UserHistory."
      });
    else res.send(data);
  });
};

// Retrieve all userHistorys from the database.
exports.findAll = (req, res) => {
  UserHistory.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving userHistorys."
      });
    else res.send(data);
  });
};

// Find a single userHistory with a userHistoryId
exports.findOne = (req, res) => {
  UserHistory.findById(req.params.userHistoryId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found UserHistory with id ${req.params.userHistoryId}.`
        });
      } else {
        res.status(500).send({
          message: `Error retrieving UserHistory with id ${req.params.userHistoryId}`
        });
      }
    } else res.send(data);
  });
};

// Update an userHistory identified by the userHistoryId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  UserHistory.updateById(
    req.params.userHistoryId,
    new UserHistory(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found UserHistory with id ${req.params.userHistoryId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating UserHistory with id " + req.params.userHistoryId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete an userHistory with the specified userHistoryId in the request
exports.delete = (req, res) => {
  UserHistory.remove(req.params.userHistoryId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found UserHistory with id ${req.params.userHistoryId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete UserHistory with id " + req.params.userHistoryId
        });
      }
    } else res.send({ message: `UserHistory was deleted successfully!` });
  });
};

// Delete all userHistorys from the database.
exports.deleteAll = (req, res) => {
  UserHistory.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all userHistorys."
      });
    else res.send({ message: `All UserHistorys were deleted successfully!` });
  });
};