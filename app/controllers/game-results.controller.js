const GameResult = require("../models/game-results.model.js");

// Create and Save a new gameResult
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a gameResult
  const gameResult = new GameResult({
    result_name: req.body.result_name,
    id_result: req.body.id_result,
  });

  // Save GameResult in the database
  GameResult.create(gameResult, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the GameResult."
      });
    else res.send(data);
  });
};

// Retrieve all gameResults from the database.
exports.findAll = (req, res) => {
  GameResult.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving gameResults."
      });
    else res.send(data);
  });
};

// Find a single gameResult with a gameResultId
exports.findOne = (req, res) => {
  GameResult.findById(req.params.gameResultId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found GameResult with id ${req.params.gameResultId}.`
        });
      } else {
        res.status(500).send({
          message: `Error retrieving GameResult with id ${req.params.gameResultId}`
        });
      }
    } else res.send(data);
  });
};

// Update an gameResult identified by the gameResultId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  GameResult.updateById(
    req.params.gameResultId,
    new GameResult(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found GameResult with id ${req.params.gameResultId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating GameResult with id " + req.params.gameResultId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete an gameResult with the specified gameResultId in the request
exports.delete = (req, res) => {
  GameResult.remove(req.params.gameResultId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found GameResult with id ${req.params.gameResultId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete GameResult with id " + req.params.gameResultId
        });
      }
    } else res.send({ message: `GameResult was deleted successfully!` });
  });
};

// Delete all gameResults from the database.
exports.deleteAll = (req, res) => {
  GameResult.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all gameResults."
      });
    else res.send({ message: `All GameResults were deleted successfully!` });
  });

};