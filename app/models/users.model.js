const pool = require("./db.js");

// constructor
const User = function (user) {
  this.username = user.username;
  this.email = user.email;
  this.password = user.password;
};

User.create = (newUser, result) => {
  pool.query("INSERT INTO user_game (id_user, username, email, password) values ((SELECT MAX(id_user)+1 FROM user_game), $1, $2, $3)", [newUser.username, newUser.email, newUser.password], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

User.findById = (userId, result) => {
  pool.query(`SELECT id_user, username, email, md5(password) as password FROM user_game WHERE id_user = ${userId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res) {
      console.log("found user: ", res);
      result(null, res);
      return;
    }

    // not found user with the id
    result({ kind: "not_found" }, null);
  });
};


User.login = (email, password, result) => {
  pool.query(`SELECT id_user, username, email, md5(password) as password FROM user_game WHERE email = '${email}' AND password = '${password}'`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res) {
      console.log("found user: ", res);
      result(null, res);
      return;
    }

    // not found user with the id
    result({ kind: "not_found" }, null);
  });
};

User.getAll = result => {
  pool.query("SELECT id_user, username, email, md5(password) as password FROM user_game", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("user_game: ", res);
    result(null, res);
  });
};

User.updateById = (id, user, result) => {
  // console.log('user', user, id);
  delete user.password;
  pool.query(
    `update user_game set username = $1, email = $2 where id_user = $3`,
    [user.username, user.email, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.rowCount == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated user: ", { id_user: id, ...user });
      result(null, { id_user: id, ...user });
    }
  );
};

User.remove = (id, result) => {
  pool.query("DELETE FROM user_game WHERE id_user = $1", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.rowCount == 0) {
      // not found user with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id_user: ", id);
    result(null, res);
  });
};

User.removeAll = result => {
  pool.query("DELETE FROM user_game", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.rowCount} user_game`);
    result(null, res);
  });
};

module.exports = User;