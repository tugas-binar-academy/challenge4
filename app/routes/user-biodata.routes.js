/**
 * @swagger
 * /userBiodatas:
 *   get:
 *     tags: [UserBiodatas]
 *     name: Find userBiodata
 *     summary: Finds all userBiodata
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: UserBiodatas object
 *       '401':
 *         description: no userBiodata found in db with that name
 *       '403':
 *         description: userBiodata from client don't match
 * 
 * /userBiodatas/{id}:
 *   get:
 *     summary: gets userBiodatas by id
 *     tags: [UserBiodatas]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of userBiodata
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: userBiodatas by its id
 *         content:
 *           application/json:
 *       400:
 *         description: userBiodata can not be found
 * /userBiodatas/{id}/delete:
 *   delete:
 *     summary: remove userBiodatas by id
 *     tags: [UserBiodatas]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of userBiodata
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: userBiodatas by its id
 *         content:
 *           application/json:
 *       400:
 *         description: userBiodata can not be found
 * /userBiodatas/add:
 *   post:
 *     summary: add userBiodatas
 *     tags: [UserBiodatas]
 *     parameters:
 *      - in: body
 *        name: userBiodata
 *        schema:
 *          type: object
 *          required:
 *            - id_user
 *            - fulname
 *            - bio
 *          properties:
 *            id_user:
 *              type: integer
 *            fulname:
 *              type: string
 *            bio:
 *              type: string
 *     responses:
 *       200:
 *         description: add userBiodatas success
 *         content:
 *           application/json:
 *       400:
 *         description: userBiodata can not be found
 *       403:
 *         description: bad request
 * /userBiodatas/{id}/update:
 *   put:
 *     summary: update userBiodatas
 *     tags: [UserBiodatas]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        required: true
 *      - in: body
 *        name: userBiodata
 *        schema:
 *          type: object
 *          required:
 *            - id_biodata
 *            - id_user
 *            - fullname
 *            - bio
 *          properties:
 *            id_biodata:
 *              type: integer
 *            id_user:
 *              type: integer
 *            fullname:
 *              type: string
 *            bio:
 *              type: string
 *     responses:
 *       200:
 *         description: update userBiodatas success
 *         content:
 *           application/json:
 *       400:
 *         description: userBiodata can not be found
 */


module.exports = app => {
  const userBiodatas = require("../controllers/user-biodata.controller.js");

  // Create a new userBiodata
  app.post("/userBiodatas/add", userBiodatas.create);

  // Retrieve all userBiodatas
  app.get("/userBiodatas", userBiodatas.findAll);

  // Retrieve a single userBiodata with userBiodataId
  app.get("/userBiodatas/:userBiodataId", userBiodatas.findOne);

  // Update a userBiodata with userBiodataId
  app.put("/userBiodatas/:userBiodataId/update", userBiodatas.update);

  // Delete a userBiodata with userBiodataId
  app.delete("/userBiodatas/:userBiodataId/delete", userBiodatas.delete);

  // delete all userBiodatas
  app.delete("/userBiodatas", userBiodatas.deleteAll);
};