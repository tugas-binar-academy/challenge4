const pool = require("./db.js");

// constructor
const Games = function (game) {
  this.id_results = game.id_results;
  this.result_name = game.result_name;
};

Games.create = (newGames, result) => {
  pool.query("INSERT INTO game_results (id_results, result_name) values ((SELECT MAX(id_results)+1 FROM game_results), $1)", [newGames.result_name], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created game: ", { id: res.insertId, ...newGames });
    result(null, { id: res.insertId, ...newGames });
  });
};

Games.findById = (gameId, result) => {
  pool.query(`SELECT id_results, result_name FROM game_results WHERE id_results = ${gameId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.rowCount !== 0) {
      console.log("found game: ", res);
      result(null, res);
      return;
    }

    // not found game with the id
    result({ kind: "not_found" }, null);
  });
};


Games.getAll = result => {
  pool.query("SELECT id_results, result_name FROM game_results order by id_results", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("game_results: ", res);
    result(null, res);
  });
};

Games.updateById = (id, game, result) => {
  pool.query(
    "UPDATE game_results SET result_name = $1, id_results = $2 WHERE id_results = $3",
    [game.result_name, game.id_results, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.rowCount === 0) {
        // not found game with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated game: ", { id_results: id, ...game });
      result(null, { id_results: id, ...game });
    }
  );
};

Games.remove = (id, result) => {
  pool.query("DELETE FROM game_results WHERE id_results = $1", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.rowCount === 0) {
      // not found game with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted game with id_results: ", id);
    result(null, res);
  });
};

Games.removeAll = result => {
  pool.query("DELETE FROM game_results", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.row} game_results`);
    result(null, res);
  });
};

module.exports = Games;