const express = require("express");
const bodyParser = require("body-parser");
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const cors = require("cors");
const port = 8080;

const app = express();

// const corsOptions = {
//   origin: function (origin, callback) {
//     if (whitelist.indexOf(origin) !== -1 || !origin) {
//       callback(null, true)
//     } else {
//       callback(new Error('Not allowed by CORS'))
//     }
//   }
// }

const swaggerDefinition = {
  info: {
    title: 'GAMES SERVICE',
    version: '1.0.0',
    description: 'created by Mifta, to completely challenge4 on binar academy',
  },
  host: `localhost:${port}`,
  basePath: '/',
};

const options = {
  swaggerDefinition,
  apis: ['./app/routes/*.js']
}
const swaggerSpec = swaggerJsdoc(options);

// app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.use('/games-service', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/swagger.json', function (req, res) {
  res.header('Content-Type', 'application/json');
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.send(swaggerSpec);
});

app.get("/home", (req, res) => {
  res.json({ message: "NodeJs CRUD Application" });
});

require("./app/routes/users.routes.js")(app);
// require("./app/routes/hotels.routes.js")(app);
require("./app/routes/games.routes.js")(app);
require("./app/routes/game-results.routes.js")(app);
require("./app/routes/user-biodata.routes.js")(app);
require("./app/routes/user-history.routes.js")(app);

// setting port to 8080, & listening for requests http request.
app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
});