--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

-- Started on 2022-04-06 10:32:16

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 19121)
-- Name: game_results; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.game_results (
    id_results integer NOT NULL,
    result_name character varying(255) NOT NULL
);


ALTER TABLE public.game_results OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 19120)
-- Name: game_results_id_results_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.game_results_id_results_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.game_results_id_results_seq OWNER TO postgres;

--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 213
-- Name: game_results_id_results_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.game_results_id_results_seq OWNED BY public.game_results.id_results;


--
-- TOC entry 212 (class 1259 OID 19114)
-- Name: games; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games (
    id_game integer NOT NULL,
    game_name character varying(255) NOT NULL
);


ALTER TABLE public.games OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 19113)
-- Name: games_id_game_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_id_game_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_id_game_seq OWNER TO postgres;

--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 211
-- Name: games_id_game_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_id_game_seq OWNED BY public.games.id_game;


--
-- TOC entry 210 (class 1259 OID 19096)
-- Name: user_game; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_game (
    id_user integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL
);


ALTER TABLE public.user_game OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 19142)
-- Name: user_game_biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_game_biodata (
    id_biodata integer NOT NULL,
    id_user bigint NOT NULL,
    fullname character varying(255) NOT NULL,
    bio character varying(255)
);


ALTER TABLE public.user_game_biodata OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 19141)
-- Name: user_game_biodata_id_biodata_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_biodata_id_biodata_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_biodata_id_biodata_seq OWNER TO postgres;

--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_game_biodata_id_biodata_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_biodata_id_biodata_seq OWNED BY public.user_game_biodata.id_biodata;


--
-- TOC entry 219 (class 1259 OID 19131)
-- Name: user_game_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_game_history (
    id_history integer NOT NULL,
    id_user integer NOT NULL,
    id_game integer NOT NULL,
    id_result integer NOT NULL
);


ALTER TABLE public.user_game_history OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 19129)
-- Name: user_game_history_id_game_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_history_id_game_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_history_id_game_seq OWNER TO postgres;

--
-- TOC entry 3371 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_game_history_id_game_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_history_id_game_seq OWNED BY public.user_game_history.id_game;


--
-- TOC entry 215 (class 1259 OID 19127)
-- Name: user_game_history_id_history_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_history_id_history_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_history_id_history_seq OWNER TO postgres;

--
-- TOC entry 3372 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_game_history_id_history_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_history_id_history_seq OWNED BY public.user_game_history.id_history;


--
-- TOC entry 218 (class 1259 OID 19130)
-- Name: user_game_history_id_result_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_history_id_result_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_history_id_result_seq OWNER TO postgres;

--
-- TOC entry 3373 (class 0 OID 0)
-- Dependencies: 218
-- Name: user_game_history_id_result_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_history_id_result_seq OWNED BY public.user_game_history.id_result;


--
-- TOC entry 216 (class 1259 OID 19128)
-- Name: user_game_history_id_user_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_history_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_history_id_user_seq OWNER TO postgres;

--
-- TOC entry 3374 (class 0 OID 0)
-- Dependencies: 216
-- Name: user_game_history_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_history_id_user_seq OWNED BY public.user_game_history.id_user;


--
-- TOC entry 209 (class 1259 OID 19095)
-- Name: user_game_id_user_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_id_user_seq OWNER TO postgres;

--
-- TOC entry 3375 (class 0 OID 0)
-- Dependencies: 209
-- Name: user_game_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_id_user_seq OWNED BY public.user_game.id_user;


--
-- TOC entry 3189 (class 2604 OID 19124)
-- Name: game_results id_results; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.game_results ALTER COLUMN id_results SET DEFAULT nextval('public.game_results_id_results_seq'::regclass);


--
-- TOC entry 3188 (class 2604 OID 19117)
-- Name: games id_game; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games ALTER COLUMN id_game SET DEFAULT nextval('public.games_id_game_seq'::regclass);


--
-- TOC entry 3187 (class 2604 OID 19099)
-- Name: user_game id_user; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game ALTER COLUMN id_user SET DEFAULT nextval('public.user_game_id_user_seq'::regclass);


--
-- TOC entry 3194 (class 2604 OID 19145)
-- Name: user_game_biodata id_biodata; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_biodata ALTER COLUMN id_biodata SET DEFAULT nextval('public.user_game_biodata_id_biodata_seq'::regclass);


--
-- TOC entry 3190 (class 2604 OID 19134)
-- Name: user_game_history id_history; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history ALTER COLUMN id_history SET DEFAULT nextval('public.user_game_history_id_history_seq'::regclass);


--
-- TOC entry 3191 (class 2604 OID 19135)
-- Name: user_game_history id_user; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history ALTER COLUMN id_user SET DEFAULT nextval('public.user_game_history_id_user_seq'::regclass);


--
-- TOC entry 3192 (class 2604 OID 19136)
-- Name: user_game_history id_game; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history ALTER COLUMN id_game SET DEFAULT nextval('public.user_game_history_id_game_seq'::regclass);


--
-- TOC entry 3193 (class 2604 OID 19137)
-- Name: user_game_history id_result; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history ALTER COLUMN id_result SET DEFAULT nextval('public.user_game_history_id_result_seq'::regclass);


--
-- TOC entry 3349 (class 0 OID 19121)
-- Dependencies: 214
-- Data for Name: game_results; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.game_results (id_results, result_name) FROM stdin;
2	lose
1	WIN
\.


--
-- TOC entry 3347 (class 0 OID 19114)
-- Dependencies: 212
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.games (id_game, game_name) FROM stdin;
2	Fintone
3	Fix San
4	Trippledex
5	Namfix
6	Hatity
7	Zoolab
8	Bytecard
9	Bitchip
10	Duobam
11	Bamity
12	Matsoft
13	Asoka
14	Ronstring
15	Flowdesk
16	Lotlux
17	Bitchip
18	Stronghold
19	Y-find
20	Treeflex
21	Tampflex
22	Tres-Zap
23	Voltsillam
24	Y-Solowarm
25	Transcof
26	Andalax
27	Overhold
28	Viva
29	Prodder
30	Namfix
1	Metal Slug
32	Tekken
31	TEKKEN
33	Kura Kura Ninja
\.


--
-- TOC entry 3345 (class 0 OID 19096)
-- Dependencies: 210
-- Data for Name: user_game; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_game (id_user, username, password, email) FROM stdin;
3	fvinau2	s2IyFbPPgTJW	kheyburn2@tripod.com
4	pconaboy3	Wt28c3ff	czaczek3@cornell.edu
5	gpreator4	k4eO1qvM3du	sgirod4@dropbox.com
6	farens5	mmrIVQbY	gbatrim5@alexa.com
7	hmauchlen6	feamor7Y	jcleft6@digg.com
8	kbirkinshaw7	6z1TxAh	bhead7@g.co
9	clangshaw8	1xdhV0PX3VWe	otring8@wp.com
10	dlamden9	kdfAKqieYzfb	abullivent9@webs.com
11	tdrayna	Volnczuf	ecasarolia@uiuc.edu
12	trosnaub	PRe29XGl	bvigursb@pen.io
13	dvanderbeekc	Ll2opZ0XKbph	vlunneyc@bandcamp.com
14	ihubbackd	stjA9IAVRYDW	ggoodsalld@woothemes.com
15	rmacphaile	SE7uNB	gibbse@desdev.cn
16	kjovovicf	WZtbzWZk8	csimsf@histats.com
17	wyashanovg	zS3Zxf25lao	nbysouthg@e-recht24.de
18	mcallejah	w42Ar8	aseggesh@ycombinator.com
19	gpretiousi	HK2oLFt9Io	amciloryi@answers.com
1	try	zLaIiLjk	xxx@gmail.com
2	aaaa	123	smm@mail
20	string	string	string
\.


--
-- TOC entry 3356 (class 0 OID 19142)
-- Dependencies: 221
-- Data for Name: user_game_biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_game_biodata (id_biodata, id_user, fullname, bio) FROM stdin;
1	1	Edvard Adami	Contusion of left little finger w damage to nail, subs
2	2	Demeter Bomfield	Gout due to renal impairment, left shoulder
3	3	Verine Drennan	Hemopericardium as current complication following AMI
4	4	Randi Minear	Military training ground as place
5	5	Valry Lewer	Subluxation of right scapula, initial encounter
6	6	Lorinda Magner	Fall into oth water striking surfc causing oth injury, subs
7	7	Milton McPhelimey	Other physeal fracture of lower end of right tibia, sequela
8	8	Lester Gegay	Drowning and submersion due to fishing boat sinking
9	9	Huntington Cannan	Subluxation of unsp acromioclavicular joint, init encntr
\.


--
-- TOC entry 3354 (class 0 OID 19131)
-- Dependencies: 219
-- Data for Name: user_game_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_game_history (id_history, id_user, id_game, id_result) FROM stdin;
2	3	2	2
3	5	3	2
4	5	4	1
5	7	5	1
6	7	6	2
7	9	7	2
8	9	8	1
9	11	9	1
10	11	10	1
11	1	1	1
\.


--
-- TOC entry 3376 (class 0 OID 0)
-- Dependencies: 213
-- Name: game_results_id_results_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.game_results_id_results_seq', 1, false);


--
-- TOC entry 3377 (class 0 OID 0)
-- Dependencies: 211
-- Name: games_id_game_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_id_game_seq', 1, false);


--
-- TOC entry 3378 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_game_biodata_id_biodata_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_biodata_id_biodata_seq', 1, false);


--
-- TOC entry 3379 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_game_history_id_game_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_history_id_game_seq', 1, false);


--
-- TOC entry 3380 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_game_history_id_history_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_history_id_history_seq', 1, false);


--
-- TOC entry 3381 (class 0 OID 0)
-- Dependencies: 218
-- Name: user_game_history_id_result_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_history_id_result_seq', 1, false);


--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 216
-- Name: user_game_history_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_history_id_user_seq', 1, false);


--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 209
-- Name: user_game_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_id_user_seq', 11, true);


--
-- TOC entry 3200 (class 2606 OID 19126)
-- Name: game_results game_results_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.game_results
    ADD CONSTRAINT game_results_pkey PRIMARY KEY (id_results);


--
-- TOC entry 3198 (class 2606 OID 19119)
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id_game);


--
-- TOC entry 3204 (class 2606 OID 19149)
-- Name: user_game_biodata user_game_biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_biodata
    ADD CONSTRAINT user_game_biodata_pkey PRIMARY KEY (id_biodata);


--
-- TOC entry 3202 (class 2606 OID 19139)
-- Name: user_game_history user_game_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history
    ADD CONSTRAINT user_game_history_pkey PRIMARY KEY (id_history);


--
-- TOC entry 3196 (class 2606 OID 19103)
-- Name: user_game user_game_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game
    ADD CONSTRAINT user_game_pkey PRIMARY KEY (id_user);


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE game_results; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.game_results TO mifta;


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE games; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.games TO mifta;


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE user_game; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.user_game TO mifta;


--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE user_game_biodata; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.user_game_biodata TO mifta;


--
-- TOC entry 3370 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE user_game_history; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.user_game_history TO mifta;


-- Completed on 2022-04-06 10:32:16

--
-- PostgreSQL database dump complete
--

