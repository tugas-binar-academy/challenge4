const UserBiodata = require("../models/user-biodata.model.js");

// Create and Save a new userBiodata
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a userBiodata
  const userBiodata = new UserBiodata({
    id_biodata: req.body.id_biodata,
    id_user: req.body.id_user,
    fullname: req.body.fullname,
    bio: req.body.bio,
  });

  // Save UserBiodata in the database
  UserBiodata.create(userBiodata, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the UserBiodata."
      });
    else res.send(data);
  });
};

// Retrieve all userBiodatas from the database.
exports.findAll = (req, res) => {
  UserBiodata.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving userBiodatas."
      });
    else res.send(data);
  });
};

// Find a single userBiodata with a userBiodataId
exports.findOne = (req, res) => {
  UserBiodata.findById(req.params.userBiodataId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found UserBiodata with id ${req.params.userBiodataId}.`
        });
      } else {
        res.status(500).send({
          message: `Error retrieving UserBiodata with id ${req.params.userBiodataId}`
        });
      }
    } else res.send(data);
  });
};

// Update an userBiodata identified by the userBiodataId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  UserBiodata.updateById(
    req.params.userBiodataId,
    new UserBiodata(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found UserBiodata with id ${req.params.userBiodataId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating UserBiodata with id " + req.params.userBiodataId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete an userBiodata with the specified userBiodataId in the request
exports.delete = (req, res) => {
  UserBiodata.remove(req.params.userBiodataId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found UserBiodata with id ${req.params.userBiodataId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete UserBiodata with id " + req.params.userBiodataId
        });
      }
    } else res.send({ message: `UserBiodata was deleted successfully!` });
  });
};

// Delete all userBiodatas from the database.
exports.deleteAll = (req, res) => {
  UserBiodata.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all userBiodatas."
      });
    else res.send({ message: `All UserBiodatas were deleted successfully!` });
  });
};